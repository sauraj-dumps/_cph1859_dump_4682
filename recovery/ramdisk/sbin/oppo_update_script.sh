#!/sbin/sh
para_list=$@

#Symlink funtion
if [ "$1" = "symlink" ]; then
    shift
    link_src=$1
    shift
    for link_dest in "$@"
    do
        if [ ${link_dest:0:1} != "/" ]; then
            echo "link dest path not start with root dir"
            exit 1
        fi

        link_dir=$(dirname ${link_dest})
        rm -rf link_dest
        cd ${link_dir}
        ln -sf ${link_src} ${link_dest}
    done
    exit 0
fi

#Delete data function
if [ "$1" = "force_update_data_app" ]; then
    app_exist=false
    shift
    apk_name=$1
    apk_new=/tmp/data/app/$apk_name/$apk_name.apk
    for path in $@
    do
        apkdir=`find /data/app -maxdepth 1  | grep $path`
        if [ -n "$apkdir" ] && [ -d $apkdir ]; then
            app_exist=true
            apk_origin_name=`ls $apkdir | grep apk`
            rm -rf $apkdir/*
            cp -f $apk_new $apkdir/$apk_origin_name
            chmod 0644 $apkdir/$apk_origin_name
            chown 1000:1000 $apkdir/$apk_origin_name
            chcon u:object_r:apk_data_file:s0 $apkdir/$apk_origin_name
            break
        fi
    done

    #Copy apk if it uninstalled.
    if [ "$app_exist" == "false" ];then
        echo "$apk_name not exist in device."
        apkdir=/data/app/$apk_name
        mkdir -p $apkdir
        cp -f $apk_new $apkdir/
        chmod 0771 $apkdir
        chmod 0644 $apkdir/*
        chown -R 1000:1000 $apkdir
        chcon u:object_r:apk_data_file:s0 $apkdir/$apk_origin_name
    fi
fi

#copy data function
if [ "$1" = "force_copy_data_app" ]; then
    app_exist=false
    shift
    apk_name=$1
    apk_new=/tmp/data/app/$apk_name/$apk_name.apk
    for path in $@
    do
        apkdir=`find /data/app -maxdepth 1  | grep $path`
        if [ -n "$apkdir" ] && [ -d $apkdir ]; then
            app_exist=true
            break
        fi
    done

    #Copy apk if it uninstalled.
    if [ "$app_exist" == "false" ];then
        echo "$force copy,apk_name not exist in device,copy"
        apkdir=/data/app/$apk_name
        mkdir -p $apkdir
        cp -f $apk_new $apkdir/
        chmod 0771 $apkdir
        chmod 0644 $apkdir/*
        chown -R 1000:1000 $apkdir
        chcon u:object_r:apk_data_file:s0 $apkdir/$apk_origin_name
    fi
fi

#Update recovery
if [ "$1" = "force_update_recovery" ];then
    dd if=/tmp/recovery.img of=/dev/block/platform/bootdevice/by-name/recovery
    echo "dd update recovery done."
fi
#Other fuction
