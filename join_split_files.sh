#!/bin/bash

cat system/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> system/priv-app/Velvet/Velvet.apk
rm -f system/priv-app/Velvet/Velvet.apk.* 2>/dev/null
cat system/priv-app/OppoGallery2/OppoGallery2.apk.* 2>/dev/null >> system/priv-app/OppoGallery2/OppoGallery2.apk
rm -f system/priv-app/OppoGallery2/OppoGallery2.apk.* 2>/dev/null
cat system/app/YouTube/YouTube.apk.* 2>/dev/null >> system/app/YouTube/YouTube.apk
rm -f system/app/YouTube/YouTube.apk.* 2>/dev/null
cat system/app/Chrome/Chrome.apk.* 2>/dev/null >> system/app/Chrome/Chrome.apk
rm -f system/app/Chrome/Chrome.apk.* 2>/dev/null
cat system/app/GoogleLatinInput/GoogleLatinInput.apk.* 2>/dev/null >> system/app/GoogleLatinInput/GoogleLatinInput.apk
rm -f system/app/GoogleLatinInput/GoogleLatinInput.apk.* 2>/dev/null
